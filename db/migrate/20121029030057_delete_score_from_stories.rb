class DeleteScoreFromStories < ActiveRecord::Migration
  def change
    remove_column :stories, :score
  end
end
