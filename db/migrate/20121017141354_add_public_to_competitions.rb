class AddPublicToCompetitions < ActiveRecord::Migration
  def change
    add_column :competitions, :public, :boolean, :default => true
  end
end
