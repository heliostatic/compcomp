class ChangeScoreToVotesOnSubmission < ActiveRecord::Migration
  def change
    rename_column :submissions, :score, :votes
  end
end
