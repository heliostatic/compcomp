class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.date :deadline

      t.timestamps
    end
  end
end
