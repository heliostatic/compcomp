class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.references :user
      t.references :story
      t.references :competition

      t.timestamps
    end
    add_index :submissions, :user_id
    add_index :submissions, :story_id
    add_index :submissions, :competition_id
  end
end
