class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.belongs_to :user
      t.text :text
      t.integer :score, :default => 0

      t.timestamps
    end
  end
end
