class AddingVoteScopeToVotes < ActiveRecord::Migration
  def up
    add_column :votes, :vote_scope, :string

    add_index :votes, [:voter_id, :voter_type, :vote_scope]
    add_index :votes, [:votable_id, :votable_type, :vote_scope]
  end

  def down
    remove_column :votes, :vote_scope
    remove_index :votes, [:voter_id, :voter_type, :vote_scope]
    remove_index :votes, [:votable_id, :votable_type, :vote_scope]
  end
end
