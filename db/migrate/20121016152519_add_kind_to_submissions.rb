class AddKindToSubmissions < ActiveRecord::Migration
  def change
    add_column :submissions, :kind, :string
  end
end
