class DropKindFromSubmissions < ActiveRecord::Migration
  def change
    remove_column :submissions, :kind
  end
end
