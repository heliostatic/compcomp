class AddTitleToCompetition < ActiveRecord::Migration
  def change
    add_column :competitions, :title, :string
  end
end
