class AddScoreToSubmissions < ActiveRecord::Migration
  def change
    add_column :submissions, :score, :integer, default: 0
  end
end
