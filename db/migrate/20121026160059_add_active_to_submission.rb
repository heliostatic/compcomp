class AddActiveToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :active, :boolean, default: true 
  end
end
