class AddRoleToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :role, :integer, default: 0
  end
end
