CompositionCompetition::Application.routes.draw do
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  authenticated :user do
    root :to => 'home#authenticated_index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users do 
    resources :stories
  end
  resources :competitions do
    resources :submissions do
      member {post :vote}
    end
  end
end