# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
CompositionCompetition::Application.initialize!

# Monkey Patch CanCan
# require "cancan_nested_hash_fix"
require 'cancan_squeel_fixes'