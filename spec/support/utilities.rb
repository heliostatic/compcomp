def sign_in(user)
  visit new_user_session_path
  within('.container') do
    fill_in "Email",    with: user.email
    fill_in "Password", with: user.password
    click_button "Sign in"
  end
  # Sign in when not using Capybara as well.
  cookies[:remember_token] = user.remember_token
end