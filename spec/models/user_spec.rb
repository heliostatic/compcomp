# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  admin                  :boolean          default(FALSE)
#

require 'spec_helper'

describe User do
  
  let(:user) { FactoryGirl.create(:user) }
  
  subject { user }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:remember_me) }
  it { should respond_to(:admin) }
  it { should respond_to(:encrypted_password) }
  it { should respond_to(:stories) }

  it { should be_valid }
  it { should_not be_admin }
  
  describe "with admin attribute set to 'true'" do
    before do
      user.save!
      user.toggle!(:admin)
    end

    it { should be_admin }
  end

  describe "accessible attributes" do
    it "should should not allow access to admin" do
      expect do
        User.new(admin: true)
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  it "should require a name" do
    no_name_user = FactoryGirl.build(:user, name: "")
    no_name_user.should_not be_valid
  end

  it "should require an email address" do
    no_email_user = FactoryGirl.build(:user, email: "")
    no_email_user.should_not be_valid
  end
  
  it "should accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    addresses.each do |address|
      valid_email_user = FactoryGirl.build(:user, email: address)
      valid_email_user.should be_valid
    end
  end
  
  it "should reject invalid email addresses" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |address|
      invalid_email_user = FactoryGirl.build(:user, email: address)
      invalid_email_user.should_not be_valid
    end
  end
  
  it "should reject duplicate email addresses" do
    user.save!
    user_with_duplicate_email = FactoryGirl.build(:user)
    user_with_duplicate_email.should_not be_valid
  end
  
  it "should reject email addresses identical up to case" do
    user_with_same_email = user.dup
    user_with_same_email.email = user.email.upcase
    user_with_same_email.should_not be_valid
  end
  
  describe "password validations" do

    it "should require a password" do
      FactoryGirl.build(:user, password: "").should_not be_valid
    end
    
    it "should reject short passwords" do
      short = "a" * 5
      FactoryGirl.build(:user, password: short).should_not be_valid
    end
  end

  describe "story associations" do
    
    before { user.save! }
    let!(:private_story) { FactoryGirl.create(:story, user: user) }

    it "should have the right stories" do
      user.stories.should == [private_story]
    end

    it "should destroy associated story" do
      stories = user.stories
      user.destroy
      stories.each do |story|
        Story.find_by_id(story.id).should be_nil
      end
    end
  end

end
