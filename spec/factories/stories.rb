# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :story do
    title "Story Title"
    text "Story text"
    public false
    user
  end
end
