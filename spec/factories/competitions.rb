# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :competition do
    owner 1
    deadline "2012-10-13 16:32:40"
  end
end
