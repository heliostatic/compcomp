require 'spec_helper'

describe StoriesController do
  describe "unauthenticated" do
    let!(:user) { FactoryGirl.create(:user) }

    it 'should allow access to public stories' do
      public_story = FactoryGirl.create(:story, public: true, user: user)
      get :show, user_id: user.id, id: public_story.id
      response.should redirect_to(user_story_path(user, public_story))
    end

    it "shouldn't allow access to a private story" do
      private_story = FactoryGirl.create(:story, user: user)
      get :show, user_id: user.id, id: private_story.id
      response.should be_redirect
    end
  end
end