module ApplicationHelper

  # Devise helpers for rendering partials elsewhere
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  # Nav link add active class
  def nav_link(link_text, link_path)
  class_name = current_page?(link_path) ? 'active' : 'nav_link'

    content_tag(:li, :class => class_name) do
      link_to link_text, link_path
    end
  end

  # Displays html content. Probably not super safe.
  def display_as_html(text, klass = nil)
    # text.html_safe
    simple_format(text, class: klass )
  end
end