# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $('.datepicker').datepicker({"format": "yyyy-mm-dd", 'todayBtn': true, 'todayHighlight': true})
  $("#competition_description").wysihtml5
    "font-styles": true #Font styling, e.g. h1, h2, etc. Default true
    emphasis: true #Italics, bold, etc. Default true
    lists: true #(Un)ordered lists, e.g. Bullets, Numbers. Default true
    html: false #Button which allows you to edit the generated HTML. Default false
    link: false #Button to insert a link. Default true
    image: false #Button to insert an image. Default true,
    color: false #Button to change color of font
    events:
      load: ->
        $(".wysihtml5-toolbar a").each ->
          $(@).attr "tabindex", "-1"

  $(".btn-group > .btn, .btn[data-toggle=\"button\"]").click ->
    if $(this).attr("class-toggle") isnt `undefined` and not $(this).hasClass("disabled")
      btnGroup = $(this).parent(".btn-group")
      if btnGroup.attr("data-toggle") is "buttons-radio"
        btnGroup.find(".btn").each ->
          $(this).removeClass $(this).attr("class-toggle")
        $(this).addClass $(this).attr("class-toggle")
      if btnGroup.attr("data-toggle") is "buttons-checkbox" or $(this).attr("data-toggle") is "button"
        if $(this).hasClass("active")
          $(this).removeClass $(this).attr("class-toggle")
        else
          $(this).addClass $(this).attr("class-toggle")