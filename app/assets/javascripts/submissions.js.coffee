# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $("#submission_story_id").select2()

# $ ->
#   $('.edit_submission a.btn').click ->
#     $(@).parents('form').submit()

$ ->
  $('div.btn-group[data-toggle-name]').each ->
    group = $(@)
    form = group.parents("form").eq(0)
    name = group.attr("data-toggle-name")
    hidden = $("input[name=\"" + name + "\"]", form)
    $("a", group).each ->
      button = $(@)
      button.on "click", ->
        unless $(@).hasClass('active')
          hidden.val $(@).attr('data-value')
          form.submit()

      # button.addClass "active"  if button.attr('data-value') is hidden.val()