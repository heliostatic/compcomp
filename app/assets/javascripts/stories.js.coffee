# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $("#story_text").wysihtml5
    "font-styles": false #Font styling, e.g. h1, h2, etc. Default true
    emphasis: true #Italics, bold, etc. Default true
    lists: false #(Un)ordered lists, e.g. Bullets, Numbers. Default true
    html: false #Button which allows you to edit the generated HTML. Default false
    link: false #Button to insert a link. Default true
    image: false #Button to insert an image. Default true,
    color: false #Button to change color of font
    events:
      load: ->
        $(".wysihtml5-toolbar a").each ->
          $(@).attr "tabindex", "-1"