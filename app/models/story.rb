# == Schema Information
#
# Table name: stories
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  text       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  title      :string(255)
#  slug       :string(255)
#  public     :boolean
#

class Story < ActiveRecord::Base
  extend FriendlyId
  attr_accessible :user_id, :text, :title, :competition, :submissions, :public
  friendly_id :title, use: :slugged

  has_paper_trail

  belongs_to :user
  has_many :submissions, :dependent => :destroy
  has_many :competitions, :through => :submissions

  validates :user, :presence => true
  validates :title, :presence => true
  validates :text, :presence => true

  def to_s
    self.title.titleize
  end
end
