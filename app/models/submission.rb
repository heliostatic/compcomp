# == Schema Information
#
# Table name: submissions
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  story_id           :integer
#  competition_id     :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  active             :boolean          default(TRUE)
#  votes              :integer          default(0)
#  role               :integer          default(0)
#  cached_votes_total :integer          default(0)
#  cached_votes_up    :integer          default(0)
#  cached_votes_down  :integer          default(0)
#

class Submission < ActiveRecord::Base
  attr_accessible :competition_id, :story_id, :role, :active, :sub_vote
  
  belongs_to :user
  belongs_to :story
  belongs_to :competition

  acts_as_votable

  ROLE = { participant: 0, owner: 1, judge: 2 }

  paginates_per 1

  scope :for_consideration, where('role' => 0)

  validates :competition, :presence => true
  validates :user, :presence => true
  validates :user_id, :uniqueness => { :scope => [:competition_id, :story_id] }, :unless => lambda { |asset| asset.story_id.nil?}

  def story_as_submitted
    unless self.story_id.nil?
      Story.find(self.story_id).version_at(self.created_at)
    end
  end

  def role
    ROLE.key(read_attribute(:role))
  end

  def role=(s)
    write_attribute(:role, ROLE[s])
  end

  def sub_vote
    @value
  end

  def sub_vote=(value)
    @value = value
  end
end
