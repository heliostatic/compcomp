class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :access, :all
    else
      # competitions
      can :read, :competitions, competitors: { id: user.id } # user is a competitor, problem: https://github.com/ryanb/cancan/issues/213
      can :read, :competitions, judges: { id: user.id } #user is a judge. same issue.
      can :read, :competitions, public: true # competition is public
      # cannot :read, :competitions, Competition.expired do |c|
      #   c.deadline.past?
      # end
      # TODO: add date/expiration based permissions that don't break with squeel and postgres and cancan 2.0
      can :vote, :competitions, judges: { id: user.id }
      can :access, :competitions, user_id: user.id # user is owner

      # submissions
      can [:read, :create, :update, :destroy], :submissions, user_id: user.id
      # can :read, :submissions, competition: { owner: user.id }
      can :read, :submissions, competition: { judges: {id: user.id } } # Issue: https://github.com/ryanb/cancan/issues/646
      can :vote, :submissions, competition: { judges: {id: user.id } }

      # stories
      can :access, :stories, user_id: user.id
      can :read, :stories, public: true

      # sessions and users
      can :access, :users, id: user.id
      can :access, :home, [:authenticated_index, :index]
      can :create, [:'devise/registrations', :'devise/sessions']
      can :destroy, [:'devise/sessions']
      can :edit, :'devise/registrations'
      can :create, :'devise/passwords'
    end
  end
end
