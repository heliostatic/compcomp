# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  admin                  :boolean          default(FALSE)
#

class User < ActiveRecord::Base
  attr_accessible :name, :email, :password, :remember_me
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_paper_trail
  acts_as_voter

  has_many :stories, :dependent => :destroy
  has_many :submissions, :dependent => :destroy
  has_many :competitions, :through => :submissions
  has_many :owned_competitions, :class_name => "Competition", :dependent => :destroy

  validates :name, :presence => true

  def all_competitions
    Competition.joins('left join submissions on submissions.competition_id = competitions.id').where('competitions.user_id = :uid OR submissions.user_id = :uid', :uid => self)
  end

  def all_competitions_expiring_today
    competitions.expiring_today | owned_competitions.expiring_today
  end

  def has_competition?(competition)
    self.competitions.exists?(competition.id) || self.owned_competitions.exists?(competition)
    # Competition.available_by(current_ability).find(competition)
  end

  def has_submitted_as_participant?(competition)
    self.submissions.for_consideration.where(:competition_id => competition).exists?
  end

  def submissions_for(competition)
    self.submissions.for_consideration.where(:competition_id => competition) || nil
  end

  def to_s
    name.titleize
  end

end
