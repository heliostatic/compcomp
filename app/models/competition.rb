# == Schema Information
#
# Table name: competitions
#
#  id          :integer          not null, primary key
#  deadline    :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string(255)
#  user_id     :integer
#  public      :boolean          default(TRUE)
#  status      :integer          default(0)
#  description :text
#  slug        :string(255)
#

class Competition < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]

  attr_accessible :deadline, :title, :user_id, :public, :description
  STATUS = { active: 0, expired: 1, inactive: 2, deleted: 3 }

  belongs_to :owner, :class_name => User.name, :foreign_key => 'user_id'
  has_many :submissions, :dependent => :destroy

  has_many :competitor_submissions, :class_name => Submission.name, :conditions => {role: 0}
  has_many :competitors, :through => :competitor_submissions, :source => :user
  has_many :stories, :through => :competitor_submissions

  has_many :judge_submissions, :class_name => Submission.name, :conditions => {role: 2}
  has_many :judges, :through => :judge_submissions, :source => :user

  scope :expired,         -> { where("DATE(deadline) < ?", Date.today) }
  scope :expiring_today,  -> { where("DATE(deadline) = ?", Date.today) }
  scope :unexpired,       -> { where("DATE(deadline) >= ?", Date.today) }

  validates :title, :presence => true
  validates :owner, :presence => true
  validates :deadline, :format => { :with => /\d{4}-\d{2}-\d{2}/, :message => "Deadline must be YYYY-MM-DD" }
  validates :deadline, :presence => true
  validate  :deadline_cannot_be_in_the_past
  validates :description, :presence => true

  after_save :create_sample_submission

  def expires_today
    self.deadline == Date.today
  end

  def expired
    self.deadline.past?
  end

  def status
    STATUS.key(read_attribute(:status))
  end

  def status=(s)
    write_attribute(:status, STATUS[s])
  end

  def to_s
    self.title.titleize
  end

  private
    def create_sample_submission
      self.owner.submissions.create!(competition_id: self.id, role: :owner, active: false)
      self.owner.submissions.create!(competition_id: self.id, role: :judge, active: false)
    end

    def deadline_cannot_be_in_the_past
      if !deadline.blank? and deadline < Date.today
        errors.add(:deadline, "The deadline can't be in the past.")
      end
    end
end