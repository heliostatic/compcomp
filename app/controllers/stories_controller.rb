class StoriesController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  (skip_before_filter :authenticate_user!, :only => [:show]) if lambda { 
    if params[:id]
      @story = Story.find(params[:user_id], params[:id])
      @story and @story.public?
    else
      false
    end
  }

  add_breadcrumb "Stories", :user_stories_path, only: %w{index show new edit create}

  def new
    @story = Story.new
    @user = current_user
    session[:return_to] ||= request.env['HTTP_X_XHR_REFERER'] || request.referer # TODO: change this when turbolinks is updated
    session[:unclobbered_return_to] = request.env['HTTP_X_XHR_REFERER']
  end

  def create
    @story = current_user.stories.build(params[:story])
    if @story.save
      flash[:success] = "Story created"
      # redirect_to [current_user, @story]
      redirect_to session.delete(:unclobbered_return_to)
    else
      @user = current_user
      render 'new'
    end
  end

  def show
    if user_signed_in?
      add_breadcrumb @story, user_story_path(current_user,@story)
    end
    if request.path != user_story_path(@story.user, @story)
      redirect_to user_story_path(@story.user, @story), status: :moved_permanently
    end
  end

  def index
    @my_stories = @stories.unscoped.where(user_id: current_user)
  end

  def edit
    add_breadcrumb @story, user_story_path(@story.user, @story)
    add_breadcrumb "Edit", edit_user_story_path(@story.user, @story)
    session[:return_to] ||= request.env['HTTP_X_XHR_REFERER'] || request.referer # TODO: change this when turbolinks is updated
  end

  def update
    if @story.save
      flash[:success] = "Story updated"
      redirect_to user_story_path(@story.user, @story)
    else
      render 'edit'
    end
  end

  def destroy
    @story.destroy
    redirect_to user_stories_path(current_user)
  end
end