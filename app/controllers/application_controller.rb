class ApplicationController < ActionController::Base
  protect_from_forgery
  enable_authorization

  add_breadcrumb "Home", :root_path
    
  rescue_from CanCan::Unauthorized do |exception|
    redirect_to root_url, :alert => exception.message
  end
end