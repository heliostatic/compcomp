class CompetitionsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  (skip_before_filter :authenticate_user!, :only => [:show]) if lambda { 
    if params[:id]
      @competition = Competition.find(params[:id])
      @competition and @competition.public?
    else
      false
    end
  }

  add_breadcrumb "Competitions", :competitions_path

  def new
    @competition = Competition.new
    session[:return_to] ||= request.env['HTTP_X_XHR_REFERER'] || request.referer # TODO: change this when turbolinks is updated
  end

  def create
    @competition = current_user.competitions.build(params[:competition])
    @competition.owner = current_user
    if @competition.save
      flash[:success] = "Competition created"
      redirect_to @competition
    else
      render :new
    end
  end

  def edit
    add_breadcrumb @competition, competition_path(@competition)
    add_breadcrumb "Edit", edit_competition_path(@competition)
    session[:return_to] ||= request.env['HTTP_X_XHR_REFERER'] || request.referer # TODO: change this when turbolinks is updated
  end

  def update
    if @competition.update_attributes(params[:competition])
      flash[:success] = "Competition updated"
      redirect_to @competition
    else
      render 'edit'
    end
  end

  def show
    add_breadcrumb @competition, competition_path(@competition)
    if request.path != competition_path(@competition)
      redirect_to @competition, status: :moved_permanently
    end
  end

  def index
    @competitions = @competitions.unexpired.uniq
  end

  def destroy
    @competition.destroy
    redirect_to competitions_path
  end
end
