class SubmissionsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource :competition
  load_and_authorize_resource :submission, :through => :competition

  add_breadcrumb "Competitions", :competitions_path

  def new
    @submission = Submission.new
    add_breadcrumb @competition, competition_path(@competition)
    add_breadcrumb 'Submit', new_competition_submission_path(@competition, @submission)
  end

  def create
    @submission = current_user.submissions.build(params[:submission])
    if @submission.save
      flash[:success] = "Submission successful"
      redirect_to @submission.competition
    else
      flash[:error] = "You've already submitted that story to this competition."
      redirect_to @submission.competition
    end
  end

  def show
    add_breadcrumb @submission.competition, competition_path(@submission.competition)
    add_breadcrumb "#{@submission.story_as_submitted.title} - by #{@submission.story_as_submitted.user}", competition_submission_path(@submission.competition, @submission)
  end 

  def index
    authorize! :vote, @competition
    add_breadcrumb @competition, competition_path(@competition)
    add_breadcrumb 'Submissions', competition_submissions_path(@competition)
    # @submissions = @submissions.for_consideration.where(:competition_id => params[:competition_id]).order(:created_at).page(params[:page]).per(1)
    @submissions = @submissions.for_consideration.order(:created_at).page(params[:page]).per(1)
  end

  def destroy
  end

  def vote
    authorize! :vote, @submission
    @submission = Submission.find(params[:id])
    logger.info "#{params[:user]} -- moo"
    value = params[:submission][:sub_vote] == '1' ? :liked_by : :disliked_by
    @submission.send(value, current_user)
    respond_to do |format|
      format.html { redirect_to :back, notice: "Thank you for voting" }
      format.js { flash.now[:success] = "Voted" }
    end
  end
end