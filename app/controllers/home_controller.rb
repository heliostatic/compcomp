class HomeController < ApplicationController

  def index
  end

  def authenticated_index
    @competitions = Competition.accessible_by(current_ability).uniq
    @expiring_competitions = @competitions.expiring_today
    @owned_competitions = current_user.owned_competitions
    @submissions = current_user.submissions.for_consideration.includes(:competition).includes(:story)
  end
end
